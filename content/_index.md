+++
title = ""
description = "Non-Profit Game Zine Initiative"
+++

Interested in local culture?
Grassroots initiatives?
Game jams (board, rpg, or more)?
Sharing your own creations and art?
Then read on!

<b>Planar Publishing</b> is a non-profit initiative towards tabletop-game-related zines.
Zines are small, easily printable and distributable publications that very literally anyone can make.
In the context of tabletop games, they can be
- micro-rpgs,
- campaign/adventure ideas,
- compact board games or card game rules,
- world-building tips,
- tutorials on your super dope technique for painting minis,

or even more artistic approaches such as
- an impassioned expression of why you like a certain game,
- your take on how games can improve a societal issue
- your own little recipe book for good RPG snacks!
Literally anything goes that you'd like to express in a short-form publication.

Planar Publishing aims to <i>encourage tabletop-game-related short-form publications</i> (generally zines), <i>compile and manage a library</i> of said creations, and <i>set up a stalls at Irish games conventions</i> to distribute them on a non-profit basis, with all proceeds going to zine authors.

<b>If you'd like to make a contribution, please let us know [at this address!](mailto:kavana21@tcd.ie)!</b> Planar Publishing is built with open-source content in mind; we're currently accepting any open-source contribution you'd like to make related to tabletop games. How about non-game-related things?? Sure!!! If you'd like it to be part of this collection then send us an email and we can talk about it.
